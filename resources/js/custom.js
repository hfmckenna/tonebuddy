// Function to add & remove products from subscription list

function addSub(value) {
    $.ajax({
        data: $(value).serialize(),
        type: 'post',
        url: window.location.href,
        headers: {
            'X-Event-Handler': 'shop:cart',
            'X-Partials': 'shop-product-content',
            'X-Requested-With': 'XMLHttpRequest'
        },

        success: function (data) {
            $.ajax({
                data: $(value).serialize(),
                type: 'post',
                url: window.location.href,
                headers: {
                    'X-Event-Handler': 'shop:product',
                    'X-Partials': 'shop-product-content',
                    'X-Requested-With': 'XMLHttpRequest'
                },

                success: function (data) {
                    $('#product-content').html(data['shop-product-content']);
                }
            });
        }
    });
};

//Conditional logic for marketing opt in to stop blank values being sent when accepts_marketing checkbox is unchecked

$(document).ready(function () {

    $('#marketing-opt-in').change(function () {
        if ($(this).is(":checked")) {
            $('#marketing-value-input').attr("value", 1);
        } else if (!$(this).is(":checked")) {
            $('#marketing-value-input').attr("value", 0);
        }
    });

});

$(document).ready(function () {
    // Shop Category Accordion

    if ($(window).width() < 991) {

        $("#sidebar-accordions").find("div[role|='button']").next('.slide').hide();

        $("#sidebar-accordions").find("div[role|='button']").click(function () {
            $(this).removeClass('active');
            var selected = $(this).next('.slide');
            selected.slideUp('fast');
            $(this).find('.plus').toggle();
            $(this).find('.minus').toggle();
            if (selected.is(":hidden")) {
                selected.slideDown('fast');
                $(this).toggleClass('active');
            }
        });

    }
    // Prevents manufacturer display getting too long on desktop
    if ($(window).width() > 990) {

        $("#manufacturer-accordion").find("div[role|='button']").next('.slide').hide();

        $("#manufacturer-accordion").find("div[role|='button']").click(function () {
            $(this).removeClass('active');
            var selected = $(this).next('.slide');
            selected.slideUp(5);
            $(this).find('.plus').toggle();
            $(this).find('.minus').toggle();
            if (selected.is(":hidden")) {
                selected.slideDown(5);
                $(this).toggleClass('active');
            }
        });

    }

});